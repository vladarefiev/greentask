# greentask README

* Установка:

`sudo apt-get update`

`sudo apt-get install python-pip python-dev build-essential`

`sudo apt-get install git`

`sudo apt-get install python3-dev`

`sudo apt-get install nginx`

`sudo apt-get install postgresql`

`sudo apt-get install libpq-dev`


* Создать виртуальное окружение

* Склонировать репозиторий

* Устновить все зависимости
`pip isntall -r requirements.txt`

* Установить uwsgi
`pip install uwsgi`

* Прогнать миграции
`python manage.py migrate`

* Собрать статику
`python manage.py collectstatic`

* Подложить конфиг nginx

```
upstream django {
    server 127.0.0.1:8001;
}

server {
    listen      8000;
    server_name your_ip;
    charset     utf-8;

    client_max_body_size 3M;

    location /static {
        alias /your/path/;

    }

    location / {
        uwsgi_pass  django;
        include     /your/path/;
    }
}
```


* в settings.py прописать настройки подключения к ДБ

* Запускаем сервер:
`uwsgi --socket :8001 --module greetask.wsgi --chmod-socket=664`

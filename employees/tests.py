from django.test import TestCase
from django.test import Client

from employees.models import Employee, Department

class EmployeeTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        it_dep = Department.objects.create(id=1, name='IT')
        manager_dep = Department.objects.create(id=2, name='Manager')
        Employee.objects.create(
            surname='Волохов',
            name='Альберт',
            patronymic='Григорьевич',
            date_birth='1970-01-01',
            email='voloh@mail.ru',
            phone='1111111',
            date_begin='2009-12-12',
            date_end=None,
            position='Специалист',
            department=it_dep,
        )
        Employee.objects.create(
            surname='Рязанов',
            name='Олег',
            patronymic='Сергеевич',
            date_birth='1970-02-01',
            email='2@mail.ru',
            phone='1111111',
            date_begin='2009-12-12',
            date_end=None,
            position='Специалист',
            department=it_dep,
        )
        Employee.objects.create(
            surname='Язов',
            name='Константин',
            patronymic='Константинович',
            date_birth='1979-01-01',
            email='1@mail.ru',
            phone='1111111',
            date_begin='2009-11-12',
            date_end=None,
            position='Специалист',
            department=manager_dep,
        )


    def test_search(self):
        it_dep = Employee.objects.filter(department__name='IT')
        self.assertEqual(len(it_dep), 2)

        request = self.client.get('/employees/search/', {'dep': 'IT'})
        object_list = request.context_data['object_list']
        self.assertEqual([i.id for i in object_list],
                         [i.id for i in it_dep])

        request = self.client.get('/employees/alphabet/')
        self.assertEqual(request.status_code, 200)
from django.db import models

# Create your models here.

class Department(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

class Employee(models.Model):
    surname = models.CharField(max_length=32)
    name = models.CharField(max_length=32)
    patronymic = models.CharField(max_length=32)
    date_birth = models.DateField()
    email = models.EmailField(unique=True)
    phone = models.PositiveIntegerField()
    date_begin = models.DateField()
    date_end = models.DateField(null=True, blank=True)
    position = models.CharField(max_length=64)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.surname + ' ' + self.name + ' ' + self.patronymic
from django.contrib import admin
from employees.models import Department, Employee


admin.site.register(Department)
admin.site.register(Employee)
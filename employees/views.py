from django.views.generic import DetailView, ListView
from employees.models import Employee
from django.db.models import Q

# Create your views here.


class EmployeeDetail(DetailView):
    model = Employee
    template_name = 'employee/detail.html'


class SearchListView(ListView):
    model = Employee
    paginate_by = 10
    template_name = 'employee/search.html'

    def get_queryset(self):
        queryset = super(SearchListView, self).get_queryset()
        department = self.request.GET.get("dep")
        date = self.request.GET.get("date")
        params = {}
        if department:
            params.update({'department__name': department})
        if date:
            params.update({'date_begin': date})
        return queryset.filter(**params)


class AlphabetListView(ListView):
    model = Employee
    template_name = 'employee/alphabet_list.html'

    @staticmethod
    def _chunk(seq, num):
        avg = len(seq) / float(num)
        out = []
        last = 0.0
        while last <= len(seq) - 1:
            out.append(
                {
                    'alpha_list': seq[int(last):int(last + avg)],
                    'alpha_label': '{0}-{1}'.format(seq[int(last)],
                                                    seq[int(last + avg - 1)])
                }
            )
            last += avg
        return out

    def __init__(self, **kwargs):
        super(AlphabetListView, self).__init__(**kwargs)
        self.char_dict = {}

    def get_queryset(self):
        queryset = super(AlphabetListView, self).get_queryset()
        alpha = self.request.GET.get('alpha')
        char_set = set()
        for obj in queryset.values('surname').order_by(
                'surname').iterator():
            if obj['surname'][0] not in char_set:
                char_set.add(obj['surname'][0])

        char_count = len(char_set) if len(char_set) < 7 else 7
        char_list = list(char_set)
        char_list.sort()
        self.char_dict = self._chunk(char_list, char_count)

        if alpha:
            query = Q()
            for letter in alpha:
                query = query | Q(surname__startswith=letter)
            return queryset.values('surname', 'id').filter(query).order_by(
                'surname')

    def get_context_data(self, **kwargs):
        context = super(AlphabetListView, self).get_context_data(**kwargs)
        context['alpha'] = self.char_dict
        return context

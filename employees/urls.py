from django.conf.urls import url
from django.contrib import admin
from django.views.generic.base import TemplateView

from employees.views import EmployeeDetail, SearchListView, AlphabetListView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^(?P<pk>[0-9]+)/$', EmployeeDetail.as_view(), name='detail'),
    url(r'^search/$', SearchListView.as_view(), name='search'),
    url(r'^alphabet/$', AlphabetListView.as_view(), name='alphabet'),
    url(r'^$', TemplateView.as_view(template_name='employee/main.html'),
        name='main'),
]